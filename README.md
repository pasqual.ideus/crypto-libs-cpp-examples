# Crypto Libs - Softwaresicherheit - WS2019/20
## Botan 2.x
Dieses Repository enthält beispielhafte Implementierungen von Botan 2.x in C++.
## Requirements
Fully installed Botan 2.x Library<br>
https://botan.randombit.net/handbook/building.html

Make <br>
Git  <br>

## Install 
<code> make all </code><br>
Builds all Files.<br>
<code> make cbox </code><br>
Builds only the cryptobox example.<br>
<code> make example </code><br>
Builds only the encryption/hashing example.<br>
<code> make clean </code><br>
Removes all .out and .o files

## Cryptobox Example CLI Usage
### To Encrypt or Decrypt a file use: 
<code>./cbox.out enc/dec password filename</code><br>
Example:<br>
<code> ./cbox.out enc example2019 test.txt </code><br>
