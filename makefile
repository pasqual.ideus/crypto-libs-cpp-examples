
CXX =g++
BUILDFLGS = $(shell botan config libs)

all: example

example: example.cpp
	@echo "Building Files..."
	$(CXX) -o example.out example.cpp $(BUILDFLGS)

cbox: cryptobox.cpp
	@echo "Building Files..."
	$(CXX) -o cbox.out cryptobox.cpp $(BUILDFLGS)


clean: 
	@echo "Removing Build Files..."
	rm -f *.out *.o
rtxt:
	@echo "Removing Cipher&Decrypt Files..."
	rm -f decrypt.txt ciphertext.txt
