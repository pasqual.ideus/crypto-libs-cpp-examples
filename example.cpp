#include <botan/rng.h>
#include <botan/auto_rng.h>
#include <botan/cipher_mode.h>
#include <botan/hex.h>
#include <botan/hash.h>
#include <iostream>

//Text to be hashed
const std::string text("Test");
//Plaintext to be encrypted
const std::string plaintext("Your great-grandfather gave this watch to your granddad for good luck. Unfortunately, Dane's luck wasn't as good as his old man's.");

//Encrypts text with AES 256 Bit,GCM
void cipherExample(){
 	Botan::AutoSeeded_RNG rng;

   	const std::vector<uint8_t> key = Botan::hex_decode("2B7E151628AED2A6ABF7158809CF4F3C2B7E151628AED2A6ABF7158809CF4F3C");
   	std::cout << plaintext << "\n";	
   	std::unique_ptr<Botan::Cipher_Mode> enc = Botan::Cipher_Mode::create("AES-256/GCM", Botan::ENCRYPTION);
   	enc->set_key(key);

   	//generate fresh nonce (IV)
   	Botan::secure_vector<uint8_t> iv = rng.random_vec(enc->default_nonce_length());

   	// Copy input data to a buffer that will be encrypted
   	Botan::secure_vector<uint8_t> pt(plaintext.data(), plaintext.data()+plaintext.length());

  	enc->start(iv);
  	enc->finish(pt);

   	std::cout << enc->name() << " with iv " << Botan::hex_encode(iv) << "\n" << Botan::hex_encode(pt) << "\n";

}

//Hashes messages with SHA-256, SHA-384 and SHA-3
void hashExample(){
	std::unique_ptr<Botan::HashFunction> hash1(Botan::HashFunction::create("SHA-256"));
   	std::unique_ptr<Botan::HashFunction> hash2(Botan::HashFunction::create("SHA-384"));
   	std::unique_ptr<Botan::HashFunction> hash3(Botan::HashFunction::create("SHA-3"));


      	//update hash computations with read data
      	hash1->update(text);
      	hash2->update(text);
      	hash3->update(text);
    
   	std::cout << "SHA-256: " << Botan::hex_encode(hash1->final()) << std::endl;
   	std::cout << "SHA-384: " << Botan::hex_encode(hash2->final()) << std::endl;
   	std::cout << "SHA-3: " << Botan::hex_encode(hash3->final()) << std::endl;

}
//Showcases the simplicity of using Botan's Cryptobox function

int main()
   {
  	cipherExample();
	hashExample();
   	return 0;
   }
