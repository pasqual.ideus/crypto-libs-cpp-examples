/*
 * Cryptobox example
 */
#include <botan/botan.h>
#include <botan/cryptobox.h>
#include <fstream>
#include <iostream>
#include <vector>

using namespace Botan;

int main(int argc, char *argv[]) {

	AutoSeeded_RNG rng;

	if (argc != 4) {
		std::cout << "Usage: cryptobox mode passw filename\n";
		return 1;
	}

	std::string mode = argv[1];
	std::string passw = argv[2];
	std::string filename = argv[3];
	std::ifstream input(filename.c_str());
	if (mode == "enc") {
		std::vector<byte> file_contents;
		while (input.good()) {
			byte filebuf[4096] = { 0 };
			input.read((char*) filebuf, sizeof(filebuf));
			size_t got = input.gcount();

			file_contents.insert(file_contents.end(), filebuf, filebuf + got);
		}

		std::string ciphertext = CryptoBox::encrypt(&file_contents[0],
				file_contents.size(), passw, rng);
		std::ofstream out("ciphertext.txt");
		out << ciphertext;
	} else {
		std::ifstream ifs(filename.c_str());
		std::string content((std::istreambuf_iterator<char>(ifs)),
				(std::istreambuf_iterator<char>()));
		std::ofstream out("decrypt.txt");
		out << CryptoBox::decrypt(content, passw);

	}

}
